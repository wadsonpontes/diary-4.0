#include "Date.h"

#include <sstream>

Date::Date():
day(0),
month(0),
year(0)
{
	
}

void Date::set_from_string(const std::string& date) {
	std::stringstream stream(date);
	char discard;

	stream >> day;
	stream >> discard;
	stream >> month;
	stream >> discard;
	stream >> year;
}

std::string Date::to_string() {
	return std::to_string(day) + "/" + std::to_string(month) + "/" + std::to_string(year);
}