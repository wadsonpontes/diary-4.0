#include "Time.h"

#include <sstream>

Time::Time():
hour(0),
minute(0),
second(0)
{
	
}

void Time::set_from_string(const std::string& time) {
	std::stringstream stream(time);
	char discard;

	stream >> hour;
	stream >> discard;
	stream >> minute;
	stream >> discard;
	stream >> second;
}

std::string Time::to_string() {
	return std::to_string(hour) + ":" + std::to_string(minute) + ":" + std::to_string(second);
}