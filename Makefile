CC = g++
CFLAGS = -std=c++11 -Wall -Iinclude

prog: clean main.o App.o Diary.o Message.o Date.o Time.o Util.o
	$(CC) -o build/Diary src/*.o $(CFLAGS)
test: clean test.o App.o Diary.o Message.o Date.o Time.o Util.o
	$(CC) -o build/Test src/*.o $(CFLAGS)
clean:
	rm -f src/*.o
test.o:
	$(CC) -o src/test.o -c src/test.cpp $(CFLAGS)
main.o:
	$(CC) -o src/main.o -c src/main.cpp $(CFLAGS)
App.o:
	$(CC) -o src/App.o -c src/App.cpp $(CFLAGS)
Diary.o:
	$(CC) -o src/Diary.o -c src/Diary.cpp $(CFLAGS)
Message.o:
	$(CC) -o src/Message.o -c src/Message.cpp $(CFLAGS)
Date.o:
	$(CC) -o src/Date.o -c src/Date.cpp $(CFLAGS)
Time.o:
	$(CC) -o src/Time.o -c src/Time.cpp $(CFLAGS)
Util.o:
	$(CC) -o src/Util.o -c src/Util.cpp $(CFLAGS)